package com.lenovo.ittools.ctd.bean.testcase;

public class TestCaseFunction {
	private Integer functionId;
	private String  functionCato;
	public Integer getFunctionId() {
		return functionId;
	}
	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}
	public String getFunctionCato() {
		return functionCato;
	}
	public void setFunctionCato(String functionCato) {
		this.functionCato = functionCato;
	}
 
}
