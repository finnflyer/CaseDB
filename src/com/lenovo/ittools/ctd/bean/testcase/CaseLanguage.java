package com.lenovo.ittools.ctd.bean.testcase;

public class CaseLanguage {
	private Integer languageID;
	private String   caseInstkey;
	private Integer  osDes;
	private Integer  localSet;
	
	public Integer getLanguageID() {
		return languageID;
	}
	public void setLanguageID(Integer languageID) {
		this.languageID = languageID;
	}
	public String getCaseInstkey() {
		return caseInstkey;
	}
	public void setCaseInstkey(String caseInstkey) {
		this.caseInstkey = caseInstkey;
	}
	public Integer getOsDes() {
		return osDes;
	}
	public void setOsDes(Integer osDes) {
		this.osDes = osDes;
	}
	public Integer getLocalSet() {
		return localSet;
	}
	public void setLocalSet(Integer localSet) {
		this.localSet = localSet;
	}

}
