package com.lenovo.ittools.ctd.bean.testcase;

public class LanguagesBean {
	private Integer languageId;
	private String lanValue;

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public String getLanValue() {
		return lanValue;
	}

	public void setLanValue(String lanValue) {
		this.lanValue = lanValue;
	}

}
