package com.lenovo.ittools.ctd.bean.testcase;

public class SearchCaseBean {
	private String caseInstkey;
	private String caseName;
	private String caseCode;
	private String version;
	private String owner;
	private Integer brandId;
	private Integer funcId;
	private Integer testModeId;
	private Integer osId;
	private String brandCato;
	private String functionCato;
	private String testModeCato;
	private String osCato;
	private Integer tpOrder;
	
	public Integer getTpOrder() {
		return tpOrder;
	}
	public void setTpOrder(Integer tpOrder) {
		this.tpOrder = tpOrder;
	}
	public String getCaseInstkey() {
		return caseInstkey;
	}
	public void setCaseInstkey(String caseInstkey) {
		this.caseInstkey = caseInstkey;
	}
	public String getCaseName() {
		return caseName;
	}
	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	public String getCaseCode() {
		return caseCode;
	}
	public void setCaseCode(String caseCode) {
		this.caseCode = caseCode;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public Integer getFuncId() {
		return funcId;
	}
	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}
	public Integer getTestModeId() {
		return testModeId;
	}
	public void setTestModeId(Integer testModeId) {
		this.testModeId = testModeId;
	}
	public Integer getOsId() {
		return osId;
	}
	public void setOsId(Integer osId) {
		this.osId = osId;
	}
	public String getBrandCato() {
		return brandCato;
	}
	public void setBrandCato(String brandCato) {
		this.brandCato = brandCato;
	}


	public String getFunctionCato() {
		return functionCato;
	}
	public void setFunctionCato(String functionCato) {
		this.functionCato = functionCato;
	}
	public String getTestModeCato() {
		return testModeCato;
	}
	public void setTestModeCato(String testModeCato) {
		this.testModeCato = testModeCato;
	}
	public String getOsCato() {
		return osCato;
	}
	public void setOsCato(String osCato) {
		this.osCato = osCato;
	}
	
	
}
