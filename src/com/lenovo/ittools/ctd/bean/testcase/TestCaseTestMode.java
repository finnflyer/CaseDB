package com.lenovo.ittools.ctd.bean.testcase;

public class TestCaseTestMode {
	
	private Integer testmodeId;
	private String testmodeCato;
	public Integer getTestmodeId() {
		return testmodeId;
	}
	public void setTestmodeId(Integer testmodeId) {
		this.testmodeId = testmodeId;
	}
	public String getTestmodeCato() {
		return testmodeCato;
	}
	public void setTestmodeCato(String testmodeCato) {
		this.testmodeCato = testmodeCato;
	}
	
}
