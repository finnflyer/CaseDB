package com.lenovo.ittools.ctd.bean.testcase;

/*
 CASEINFOINSTSKEY
CASEINSTKEY
EXECUTETIME
BRANDID
FUNCID
TESTMODEID
OSID
COMPONEROWNER
AUTOMATION
HARDWAREINFO
LANGUAGECOMMENT
COMMENTS ---HW comment
MODIFYREASON
INTRODUCTION

 */
public class TestCaseInfo {
	private String caseInfoInstkey;
	private String caseInstkey;
	private Integer executeTime;
	private Integer brandId;
	private Integer funcId;
	private Integer testModeId;
	private Integer osId;
	private String componerOwner;
	private String automation;
	private String hardwareInfo;
	private String languageComment;
	private String comments;
	private String modifyReason;
	private String introduction;
	private String brandCato;
	private String funcCato;
	private String osCato;
	
	
	
	public String getBrandCato() {
		return brandCato;
	}
	public void setBrandCato(String brandCato) {
		this.brandCato = brandCato;
	}
	public String getFuncCato() {
		return funcCato;
	}
	public void setFuncCato(String funcCato) {
		this.funcCato = funcCato;
	}
	public String getOsCato() {
		return osCato;
	}
	public void setOsCato(String osCato) {
		this.osCato = osCato;
	}
	public String getCaseInfoInstkey() {
		return caseInfoInstkey;
	}
	public void setCaseInfoInstkey(String caseInfoInstkey) {
		this.caseInfoInstkey = caseInfoInstkey;
	}
	public String getCaseInstkey() {
		return caseInstkey;
	}
	public void setCaseInstkey(String caseInstkey) {
		this.caseInstkey = caseInstkey;
	}
	public Integer getExecuteTime() {
		return executeTime;
	}
	public void setExecuteTime(Integer executeTime) {
		this.executeTime = executeTime;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public Integer getFuncId() {
		return funcId;
	}
	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}
	public Integer getTestModeId() {
		return testModeId;
	}
	public void setTestModeId(Integer testModeId) {
		this.testModeId = testModeId;
	}
	public Integer getOsId() {
		return osId;
	}
	public void setOsId(Integer osId) {
		this.osId = osId;
	}
	public String getComponerOwner() {
		return componerOwner;
	}
	public void setComponerOwner(String componerOwner) {
		this.componerOwner = componerOwner;
	}
	public String getAutomation() {
		return automation;
	}
	public void setAutomation(String automation) {
		this.automation = automation;
	}
	public String getHardwareInfo() {
		return hardwareInfo;
	}
	public void setHardwareInfo(String hardwareInfo) {
		this.hardwareInfo = hardwareInfo;
	}
	public String getLanguageComment() {
		return languageComment;
	}
	public void setLanguageComment(String languageComment) {
		this.languageComment = languageComment;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getModifyReason() {
		return modifyReason;
	}
	public void setModifyReason(String modifyReason) {
		this.modifyReason = modifyReason;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	
	

}
