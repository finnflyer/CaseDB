package com.lenovo.ittools.ctd.bean.testcase;

public class TestCaseSupportOs {
	
	private Integer osId;
	private String  osDes;
	public Integer getOsId() {
		return osId;
	}
	public void setOsId(Integer osId) {
		this.osId = osId;
	}
	public String getOsDes() {
		return osDes;
	}
	public void setOsDes(String osDes) {
		this.osDes = osDes;
	}
	
}
