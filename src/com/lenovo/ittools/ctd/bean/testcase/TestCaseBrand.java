package com.lenovo.ittools.ctd.bean.testcase;

/**
 * @author Chill
 *
 */
public class TestCaseBrand {
	private Integer brandId;
	private String brandCato;
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public String getBrandCato() {
		return brandCato;
	}
	public void setBrandCato(String brandCato) {
		this.brandCato = brandCato;
	}
	
}

