package com.lenovo.ittools.ctd.bean.common;

import java.util.Date;

public class TestToolBean {
		public String instkey;
		public String toolName;
		public String description;
		public String owner;
		public String status;
		public String uploadFileName;
		public String path;
		public Date uploadTime;
		public String getInstkey() {
			return instkey;
		}
		public void setInstkey(String instkey) {
			this.instkey = instkey;
		}
		public String getToolName() {
			return toolName;
		}
		public void setToolName(String toolName) {
			this.toolName = toolName;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getOwner() {
			return owner;
		}
		public void setOwner(String owner) {
			this.owner = owner;
		}
		public Date getUploadTime() {
			return uploadTime;
		}
		public void setUploadTime(Date uploadTime) {
			this.uploadTime = uploadTime;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getUploadFileName() {
			return uploadFileName;
		}
		public void setUploadFileName(String uploadFileName) {
			this.uploadFileName = uploadFileName;
		}
		public String getPath() {
			return path;
		}
		public void setPath(String path) {
			this.path = path;
		}
		
}
